class Calculator{
    angka: number;

    constructor(_angka: number){
        this.angka = _angka;
    }

    add(penjumlah:number){
        this.angka = this.angka + penjumlah;
        return this.angka;
    }

    substract(pengurang: number){
        this.angka = this.angka - pengurang; 
        return this.angka;
    }

    devide(pembagi:number){
        this.angka = this.angka / pembagi;
        return this.angka;
    }

    multiply(pengali:number){
        this.angka = this.angka * pengali;
        return this.angka;
    }
}

class ScientificCalculator extends Calculator{
    // angka: number;
    hasil: number;

    constructor(_angka: number, ) {
        super(_angka);
        this.hasil = _angka;
    }

    add(penambah: number) {    
        this.hasil = super.add(penambah);   
        return this.hasil;
    }

    substract(pengurang: number) {
        this.hasil = super.substract(pengurang);
        return this.hasil;
    }

    mutiply(pengali: number) {
        this.hasil = super.multiply(pengali);
        return this.hasil;
    }

    devide(pembagi: number) {
        this.hasil = super.devide(pembagi);
        return this.hasil;
    }

    pi(){return Math.PI;}

    cos() {
        this.hasil = Math.cos(this.hasil);
        return this.hasil
    }

    sin() {   
        this.hasil =  Math.sin(this.hasil);
        return this.hasil;
    }

    tan() {
        this.hasil = Math.tan(this.hasil);
        return this.hasil
    }

    cosh() {
        this.hasil = (Math.pow(Math.E,this.hasil) + Math.pow(Math.E,(this.hasil*-1)))/2;
        return this.hasil;
    }

    sinh() {
        this.hasil = (Math.pow(Math.E,this.hasil) + Math.pow(Math.E,(this.hasil*-1)))/2;
        return this.hasil;
    }

    tanh() {
        this.hasil = this.sinh() / this.cosh(); 
        return this.hasil;
    }

    exp() {
        this.hasil = Math.exp(this.hasil);
        return this.hasil;
    }

    floor() {
        this.hasil = Math.floor(this.hasil);
        return this.hasil;
    }

    power(pangkat: number) {
        this.hasil = Math.pow(this.hasil, pangkat);
        return this.hasil;
    }

    squareRoot() {
        this.hasil = Math.sqrt(this.hasil);
        return this.hasil;
    }

    logTwo() {
        this.hasil = Math.log(this.hasil);
        return this.hasil;
    }

    logTen() {
        this.hasil = Math.log(this.hasil) / Math.LN10;
        return this.hasil;
    }

    ceiling() {
        this.hasil = Math.ceil(this.hasil);
        return this.hasil;
    }
}