var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Calculator = /** @class */ (function () {
    function Calculator(_angka) {
        this.angka = _angka;
    }
    Calculator.prototype.add = function (penjumlah) {
        this.angka = this.angka + penjumlah;
        return this.angka;
    };
    Calculator.prototype.substract = function (pengurang) {
        this.angka = this.angka - pengurang;
        return this.angka;
    };
    Calculator.prototype.devide = function (pembagi) {
        this.angka = this.angka / pembagi;
        return this.angka;
    };
    Calculator.prototype.multiply = function (pengali) {
        this.angka = this.angka * pengali;
        return this.angka;
    };
    return Calculator;
}());
var ScientificCalculator = /** @class */ (function (_super) {
    __extends(ScientificCalculator, _super);
    function ScientificCalculator(_angka) {
        var _this = _super.call(this, _angka) || this;
        _this.hasil = _angka;
        return _this;
    }
    ScientificCalculator.prototype.add = function (penambah) {
        this.hasil = _super.prototype.add.call(this, penambah);
        return this.hasil;
    };
    ScientificCalculator.prototype.substract = function (pengurang) {
        this.hasil = _super.prototype.substract.call(this, pengurang);
        return this.hasil;
    };
    ScientificCalculator.prototype.mutiply = function (pengali) {
        this.hasil = _super.prototype.multiply.call(this, pengali);
        return this.hasil;
    };
    ScientificCalculator.prototype.devide = function (pembagi) {
        this.hasil = _super.prototype.devide.call(this, pembagi);
        return this.hasil;
    };
    ScientificCalculator.prototype.pi = function () { return Math.PI; };
    ScientificCalculator.prototype.cos = function () {
        this.hasil = Math.cos(this.hasil);
        return this.hasil;
    };
    ScientificCalculator.prototype.sin = function () {
        this.hasil = Math.sin(this.hasil);
        return this.hasil;
    };
    ScientificCalculator.prototype.tan = function () {
        this.hasil = Math.tan(this.hasil);
        return this.hasil;
    };
    ScientificCalculator.prototype.cosh = function () {
        this.hasil = (Math.pow(Math.E, this.hasil) + Math.pow(Math.E, (this.hasil * -1))) / 2;
        return this.hasil;
    };
    ScientificCalculator.prototype.sinh = function () {
        this.hasil = (Math.pow(Math.E, this.hasil) + Math.pow(Math.E, (this.hasil * -1))) / 2;
        return this.hasil;
    };
    ScientificCalculator.prototype.tanh = function () {
        this.hasil = this.sinh() / this.cosh();
        return this.hasil;
    };
    ScientificCalculator.prototype.exp = function () {
        this.hasil = Math.exp(this.hasil);
        return this.hasil;
    };
    ScientificCalculator.prototype.floor = function () {
        this.hasil = Math.floor(this.hasil);
        return this.hasil;
    };
    ScientificCalculator.prototype.power = function (pangkat) {
        this.hasil = Math.pow(this.hasil, pangkat);
        return this.hasil;
    };
    ScientificCalculator.prototype.squareRoot = function () {
        this.hasil = Math.sqrt(this.hasil);
        return this.hasil;
    };
    ScientificCalculator.prototype.logTwo = function () {
        this.hasil = Math.log(this.hasil);
        return this.hasil;
    };
    ScientificCalculator.prototype.logTen = function () {
        this.hasil = Math.log(this.hasil) / Math.LN10;
        return this.hasil;
    };
    ScientificCalculator.prototype.ceiling = function () {
        this.hasil = Math.ceil(this.hasil);
        return this.hasil;
    };
    return ScientificCalculator;
}(Calculator));
